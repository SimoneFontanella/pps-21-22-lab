package lab01.tdd;

public class FactoryProducer {
    public static AbstractFactory getFactory(){
        return new StrategyFactory();
    }
}