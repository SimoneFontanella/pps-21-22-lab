package lab01.tdd;

public interface AbstractFactory {
    SelectStrategy getStrategy(String strategy, int number);
}
