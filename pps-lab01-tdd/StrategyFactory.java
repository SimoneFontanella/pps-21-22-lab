package lab01.tdd;

public class StrategyFactory implements AbstractFactory {
    @Override
    public SelectStrategy getStrategy(String strategy, int number) {
        switch (strategy) {
            case "EVEN":
                return new EvenStrategy(number);
            case "EQUALS":
                return new EqualsStrategy(number);
            case "MULTIPLEOF":
                return new MultipleOfStrategy(number);
        }
        return null;
    }
}
