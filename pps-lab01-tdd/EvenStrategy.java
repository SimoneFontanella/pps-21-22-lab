package lab01.tdd;

public class EvenStrategy implements SelectStrategy {
    private int number;
    public EvenStrategy(int number) {
       this.number = number;
    }

    @Override
    public boolean apply(int element) {
        return element % number == 0;
    }
}
