package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private int head = 0;
    private List<Integer> circularList;
    private int pointer;
    private int counter = 0;

    public CircularListImpl() {
        this.circularList = new ArrayList();
        pointer = head;
    }

    @Override
    public void add(int element) {
        this.circularList.add(element);
    }

    @Override
    public int size() {
        return circularList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.circularList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        return Optional.of(this.circularList.get(pointer++));
    }

    @Override
    public Optional<Integer> previous() {
        if (pointer == head && circularList.size() > 1) {
            pointer = circularList.size() - 1;
        }
        return Optional.of(this.circularList.get(pointer--));
    }

    @Override
    public void reset() {
       pointer = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if (counter == circularList.size())
            return Optional.empty();
        counter++;
        Optional<Integer> result = this.next();
        if (strategy.apply(result.get())) {
            counter = 0;
            return result;
        } else {
            return this.next(strategy);
        }
    }
}
