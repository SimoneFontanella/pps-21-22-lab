import lab01.tdd.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.testng.annotations.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {
    protected CircularList circularList;
    AbstractFactory abstractFactory;

    @BeforeEach
    public void setUp() {
        circularList = new CircularListImpl();
        abstractFactory = FactoryProducer.getFactory();
    }

    @Test
    void testIsEmpty() {
        assertEquals(true, circularList.isEmpty());
    }

    @Test
    void testSize() {
        assertEquals(0, circularList.size());
    }

    @Test
    void testAdd() {
        circularList.add(1);
        assertEquals(1, circularList.next().get());
    }

    @Test
    void testNext() {
        circularList.add(0);
        circularList.add(1);
        circularList.add(2);
        for (int i = 0; i < circularList.size(); i++) {
            assertEquals(i, circularList.next().get());
        }
    }

    @Test
    void testPrevious() {
        circularList.add(0);
        circularList.add(1);
        circularList.add(2);
        for (int i = 2; i > 0; i--) {
            assertEquals(i, circularList.previous().get());
        }
    }

    @Test
    void testEvenStrategy() {
        SelectStrategy strategy = abstractFactory.getStrategy("EVEN", 2);
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.next(strategy).get());
    }

    @Test
    void testMultipleOfStrategy() {
        SelectStrategy strategy = abstractFactory.getStrategy("MULTIPLEOF", 2);
        circularList.add(1);
        circularList.add(4);
        assertEquals(4, circularList.next(strategy).get());
    }

    @Test
    void testEqualsStrategy() {
        SelectStrategy strategy = abstractFactory.getStrategy("EQUALS", 2);
        circularList.add(1);
        circularList.add(4);
        circularList.add(2);
        assertEquals(2, circularList.next(strategy).get());
    }

    @Test
    void testWrongEqualsStrategy() {
        SelectStrategy strategy = new EqualsStrategy(0);
        circularList.add(1);
        circularList.add(4);
        circularList.add(2);
        assertEquals(java.util.Optional.empty(), circularList.next(strategy));
    }
}
