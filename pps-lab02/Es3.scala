package test

object Es3 extends App :
  //a
  def parity(x: Int): String = x match
   case x if x % 2 == 0 => "even"
   case _ => "odd" // otherwise

  val parity2: Int => String = x => if (x % 2 == 0) "even" else "odd"

  println(parity(5))
  println(parity2(10))
  println("")

  //b
  val empty: String => Boolean = _ == "" // predicate on strings
  def neg(x: String => Boolean): String => Boolean = !x(_)
  val neg2: (x: String => Boolean) => (String => Boolean) = x => !x(_)
  val notEmpty = neg(empty) // which type of notEmpty?
  
  println(notEmpty("foo")) // true
  println(notEmpty(""))
  println(notEmpty("foo") && !notEmpty(""))

  println("")

  val notEmpty2 = neg2(empty)
  println(notEmpty2("foo")) // true
  println(notEmpty2(""))
  println(notEmpty2("foo") && !notEmpty2(""))

  println("")
  
  //c
  val empty3: Int => Boolean = _ == 0 // predicate on strings
  def neg3[X](x: X => Boolean): X => Boolean = !x(_)
  val notEmpty3 = neg3(empty3)
  println(notEmpty3(1))
  println(notEmpty3(0))