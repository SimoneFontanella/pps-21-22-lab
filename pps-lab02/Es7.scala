package test

object Es7 extends App :
  enum Shape:
    case Square(side: Int)
    case Circle(radius: Int)
    case Rectangle(base: Int, height: Int)

  object Shape:
    def perimeter(shape: Shape): Double = shape match
      case Square(s) => s * 4
      case Rectangle(b, h) => (b * 2) + (2 * h)
      case Circle(r) => (r * 2) * 3.14;

    def area(shape: Shape): Double = shape match
      case Square(s) => s * s
      case Rectangle(b, h) => b * h
      case Circle(r) => (r * r) * 3.14;







