package test

object Es5 extends App :
  def compose(f: Int => Int, g: Int => Int): Int => Int =
    n => f(g(n))

  println(compose(_ - 1, _ * 2)(5))

  def composeGeneric[X](f: X => X, g: X => X): X => X =
    n => f(g(n))

  println(composeGeneric[Int](_ - 1, _ * 2)(5))
  println(composeGeneric[String](_ + "c", _ + "b")("a"))

  //il vincolo è che devono essere tutti dello stesso tipo
  //signature => def composeGeneric[X](f: X => X, g: X => X): X => X