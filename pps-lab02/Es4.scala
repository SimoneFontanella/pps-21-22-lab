package test

object Es4 extends App :

  def p4(x: Int, y: Int, z: Int): Boolean = x match
    case x if x <= y => y <= z
    case _ => false


  println(p4(1, 2, 3))
  println(p4(1, 2, 1))

  println("")

  def p3(x: Int)(z: Int)(y: Int): Boolean = x match
    case x if x <= y => y <= z
    case _ => false

  val compareInRange: Int => Boolean = p3(1)(5)
  println(compareInRange(2))

  println("")

  val p2 = (x: Int, y: Int, z: Int) => x <= y && y <= z
  println(p2(1, 2, 3))
  println(p2(1, 2, 1))

  println("")
  val p1: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  println(p1(1)(2)(3))
  println(p1(1)(2)(1))
