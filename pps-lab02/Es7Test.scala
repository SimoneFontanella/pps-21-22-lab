package test

import org.junit.*
import org.junit.Assert.*
import exercise.Seven.Shape.*

class Es7Test:

  val square = Square(2)
  val rectangle = Rectangle(2, 3)
  val circle = Circle(2)

  @Test def testSquarePerimeter() =
    assertEquals(8.0, perimeter(square), 0)

  @Test def testSquareArea() =
    assertEquals(4.0, area(square), 0)

  @Test def testCirclePerimeter() =
    assertEquals(12.56, perimeter(circle), 0)

  @Test def testCircleArea() =
    assertEquals(12.56, area(circle), 0)

  @Test def testRectanglePerimeter() =
    assertEquals(10.0, perimeter(rectangle), 0)

  @Test def testRectangleArea() =
    assertEquals(6.0, area(rectangle), 0)