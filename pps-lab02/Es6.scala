package test

object Es6 extends App :
  def fib(n: Int): Int = n match
    case 0 => 0
    case 1 => 1
    case n => fib(n - 1) + fib(n - 2)

  println(fib(6))

  def fib2(n: Int): Int =
    @annotation.tailrec // checks only if optimisation is possible
    def _fib(acc: Int, increment: Int, n:Int): Int = n match
      case 0 => acc
      case _ => _fib(increment, acc+increment, n-1)

    _fib(0,1,n)

  println(fib2(8))