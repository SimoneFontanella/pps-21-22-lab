package u05lab.ex2

import java.util

enum Question:
  case RELEVANCE
  case SIGNIFICANCE
  case CONFIDENCE
  case FINAL

trait ConferenceReviewing:

  object ConferenceReviewing:
    def apply(): ConferenceReviewing = ConferenceReviewingImpl()

  def loadReview(article: Int, scores: Map[Question, Int]): Unit

  def loadReview(article: Int, relevance: Int, significance: Int, confidence: Int, fin: Int): Unit

  def orderedScores(article: Int, question: Question): List[Int]

  def averageFinalScore(article: Int): Double

  def acceptedArticles: Set[Int]

  def sortedAcceptedArticles: List[(Int, Double)]

  def averageWeightedFinalScoreMap: Map[Int, Double]

case class ConferenceReviewingImpl() extends ConferenceReviewing :
  var reviews: List[(Int, Map[Question, Int])] = List.empty

  override def loadReview(article: Int, scores: Map[Question, Int]): Unit =
    if scores.size < Question.values.length then
      throw new IllegalArgumentException()
    else
      reviews = reviews :+ (article, scores)

  override def loadReview(article: Int, relevance: Int, significance: Int, confidence: Int, fin: Int): Unit =
    var map: Map[Question, Int] = Map.empty
    map = map + (Question.RELEVANCE -> relevance)
    map = map + (Question.SIGNIFICANCE -> significance)
    map = map + (Question.CONFIDENCE -> confidence)
    map = map + (Question.FINAL -> fin)
    reviews = reviews :+ (article, map)

  override def orderedScores(article: Int, question: Question): List[Int] =
    reviews.filter(p => p._1 == article)
      .map(p => p._2(question))
      .sorted

  override def averageFinalScore(article: Int): Double =
    reviews.filter(p => p._1 == article)
      .map(p => p._2(Question.FINAL))
      .foldLeft(0.0)(_ + _) / reviews.count(_._1 == article)

  override def acceptedArticles: Set[Int] =
    reviews.map(q => q._1)
      .distinct
      .filter(article => averageFinalScore(article) > 5.0 &&
        reviews.filter(p => p._1 == article).map(_._2(Question.RELEVANCE)).exists(_ >= 8)).toSet

  override def sortedAcceptedArticles: List[(Int, Double)] =
    this.acceptedArticles
      .map(a => (a, this.averageFinalScore(a)))
      .toList
      .sorted((e1, e2) => e1._2 compareTo e2._2)

  override def averageWeightedFinalScoreMap: Map[Int, Double] =
    reviews.map(_._1)
      .distinct
      .map(art => art ->
        reviews.filter(a => a._1 == art)
          .map(p => p._2(Question.FINAL) * p._2(Question.CONFIDENCE) / 10.0)
          .sum /
          reviews.count(a => a._1 == art))
      .toMap

