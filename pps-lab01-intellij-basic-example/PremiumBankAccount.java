package lab01.example.model;

/**
 * This class represent a particular instance of a BankAccount.
 * In particular, a Simple Bank Account allows always the deposit
 * while the withdrawal is allowed only if the balance greater or equal the withdrawal amount
 */
public class PremiumBankAccount extends AbstractBankAccount {

    public PremiumBankAccount(AccountHolder holder, double balance, int fee) {
        super(balance, holder, fee);
    }

    @Override
    protected boolean isWithdrawAllowed(double amount) {
        return true;
    }
}
