package lab01.example.model;

public abstract class AbstractBankAccount implements BankAccount {
    protected final AccountHolder holder;
    protected double balance;
    private final int fee;

    public AbstractBankAccount(final double balance, final AccountHolder holder, int fee) {
        this.balance = balance;
        this.holder = holder;
        this.fee = fee;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }


    public final void deposit(final int userID, final double amount) {
        if (checkUser(userID)) {
            this.balance += amount - this.fee;
        }
    }

    public final void withdraw(final int userID, final double amount) {
        if (checkUser(userID) && isWithdrawAllowed(amount)) {
            this.balance -= (amount + this.fee);
        }
    }

    protected boolean isWithdrawAllowed(double amount) {
        return this.balance >= amount;
    }

    protected final boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }
}
