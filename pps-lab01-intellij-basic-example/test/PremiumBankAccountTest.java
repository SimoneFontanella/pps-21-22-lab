import lab01.example.model.AccountHolder;
import lab01.example.model.PremiumBankAccount;
import lab01.example.model.SimpleBankAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class PremiumBankAccountTest extends AbstractBankAccountTest {

    @BeforeEach
    @Override
    public void setUp() {
        this.accountHolder = new AccountHolder("Mario", "Rossi", 1);
        this.bankAccount = new PremiumBankAccount(accountHolder, 0, 0);
        this.FEE=0;
    }

    @Test
    void testPremiumWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 200);
        assertEquals(-100, bankAccount.getBalance());
    }
}
