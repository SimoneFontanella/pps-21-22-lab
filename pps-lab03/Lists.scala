package u03

import u02.Modules.isStudent

object Lists extends App :

  // A generic linkedlist
  enum List[E]:
    case Cons(head: E, tail: List[E])
    case Nil()

  // a companion object (i.e., module) for List
  object List:

    def sum(l: List[Int]): Int = l match
      case Cons(h, t) => h + sum(t)
      case _ => 0

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match
      case Cons(h, t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()

    def drop[A](l: List[A], n: Int): List[A] = l match
      case Cons(h, t) if n > 0 => drop(t, n - 1)
      case Cons(h, t) => Cons(h, t)
      case Nil() => Nil()

    def append[A](left: List[A], right: List[A]): List[A] = left match
      case Cons(h, t) => Cons(h, append(t, right))
      case Nil() => right

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case Nil() => Nil()

    /* def filterWithFlatMap[A](l: List[A])(pred: A => List[Boolean]): List[A] =
       flatMap(l)(v=> Cons(pred,Nil()))
    ? non ho capito cosa dovrebbe ritornare una  filter in termini di flatMap
     def mapWithFlatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match
       flatMap(l,f)*/

    import u02.Optionals.Option

    @annotation.tailrec
    def max(l: List[Int]): Option[Int] = l match
      case Cons(h, t) if filter(t)(_ > h) == Nil() => Option.Some(h)
      case Cons(h, t) => max(t)
      case _ => Option.None()

    import u02.Modules.Person

    def getCoursesByTeacher(persons: List[Person]): List[String] = persons match
      case Cons(h, t) => map(filter(persons)(!isStudent(_)))({ case Person.Teacher(n, c) => c; })
      case _ => Nil()

    def foldLeft[A, B](l: List[A])(acc: B)(f: (B, A) => B): B = l match
      case Cons(h, t) => f(foldLeft(t)(acc)(f), h)
      case _ => acc

    def foldRight[A, B](l: List[A])(acc: B)(f: (A, B) => B): B = l match
      case Cons(h, t) => f(h, foldRight(t)(acc)(f))
      case _ => acc


  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60

  import List.*

  println(sum(map(filter(l)(_ >= 20))(_ + 1))) // 21+31 = 52
