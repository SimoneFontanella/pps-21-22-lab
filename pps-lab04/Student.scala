package u04lab.code

import List.*

trait Student:
  def name: String

  def year: Int

  def enrolling(course: Course): Unit // the student participates to a Course

  def courses: List[String] // names of course the student participates to

  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?

trait Course:
  def name: String

  def teacher: String

object Student:
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)

object Course:
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)

case class CourseImpl(override val name: String, override val teacher: String
                     ) extends Course

case class StudentImpl(override val name: String, override val year: Int
                      ) extends Student :

  private var listCourses: List[Course] = Nil()

  override def enrolling(course: Course): Unit = listCourses = Cons(course, listCourses)

  def enrollingNCourses(course: Course*): Unit = course.foreach(c => enrolling(c))

  override def courses: List[String] = List.map(listCourses)(c => c.name)

  override def hasTeacher(teacher: String): Boolean = List.contains(List.map(listCourses)(c => c.teacher), teacher)


object factoryList:
  def apply[A](arguments: A*): List[A] =
    var list: List[A] = Nil()
    arguments.foreach(p => list = Cons(p, list))
    list

object sameTeacher:
  def unapply(courses: List[Course]): Option[String] =
    val firstTeacher: String = foldLeft(map(take(courses, 1))(t => t.teacher))("")((a, b) => b)
    val commonTeachers: List[String] = filter(map(courses)(c => c.teacher))(t => t == firstTeacher)
    if length(commonTeachers) == length(courses) then Option.Some(firstTeacher) else Option.None()


@main def checkStudents(): Unit =
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "D'Angelo")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") // defaults to 2017
  val s4 = new StudentImpl("Simone", 2017) // defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  s4.enrollingNCourses(cPPS, cPCD, cSDR)
  println(
    (s1.courses, s2.courses, s3.courses, s4.courses)
  ) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true

  //optional exercise
  val c1 = Course("PPS", "Viroli")
  val c2 = Course("PCD", "Viroli")
  val c3 = Course("SDR", "Viroli")
  val courses = factoryList(c1, c2, c3)

  courses match
    case sameTeacher(t) => println(s" $courses have same teacher $t")
    case _ => println(s" $courses have different teachers ")
  /*
                            !!! Errore!!!
  Wrong number of argument patterns for u04lab.code.sameTeacher; expected: ()
  case sameTeacher(t) => println(s" $courses have same teacher $t")

  Viene generato questa errore di compilazione, non capisco cosa sto sbagliando prof.
  */


