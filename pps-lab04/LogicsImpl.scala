package u04lab.polyglot.a01a

import Logics.*
import u04lab.polyglot.Pair
import u04lab.code.List
import u04lab.code.List.length
import scala.util.Random

/** solution and descriptions at https://bitbucket.org/mviroli/oop2019-esami/src/master/a01a/sol2/ */
class LogicsImpl(private val size: Int, private val boat: Int) extends Logics :

  private var hitList: List[Pair[Int, Int]] = List.Nil()

  private val r = Random()
  private var boatRow: Int = r.nextInt(size);
  private var boatLeftCol: Int = r.nextInt(size - boat + 1);
  private var failures: Int = 0;
  private val FAILURES: Int = 20

  println("x = " + this.boatLeftCol + " y = " + this.boatRow)

  def hit(row: Int, col: Int) =
    if row == this.boatRow && col >= this.boatLeftCol && col < this.boatLeftCol + boat then {
      this.hitList = List.Cons(Pair(row, col), this.hitList);
      if (List.length(this.hitList) == this.boat) Result.WON else Result.HIT
    }
    else {
      this.failures = this.failures + 1
      if this.failures == FAILURES then Result.LOST else Result.MISS
    }